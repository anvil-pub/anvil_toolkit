CHANGELOG
==============

### 1.4.3

###### Features
* Changed the Form template suggestion to use the correct Form ID.

### 1.4.2

###### Features
* Added an additional Taxonomy Term template suggestion, based on just the view mode.

### 1.4.1

###### Features
* [[#19](https://gitlab.com/anvil-pub/anvil_toolkit/-/merge_requests/19)] Refactor video specific url enhancer to general file enhancer.

### 1.4.0

###### Features
* [[#17](https://gitlab.com/anvil-pub/anvil_toolkit/-/merge_requests/17)] Add the anvil_toolkit_audit_trail submodule.

###### Bugfixes
* [[#18](https://gitlab.com/anvil-pub/anvil_toolkit/-/merge_requests/18)] Add the anvil_toolkit dependency to all submodules.


### 1.3.1

###### Features
* [[#16](https://gitlab.com/anvil-pub/anvil_toolkit/-/merge_requests/16)] Add the JSON:API video file url enhancer.

### 1.3.0

###### Features
* [[#15](https://gitlab.com/anvil-pub/anvil_toolkit/-/merge_requests/15)] Add the anvil_toolkit_jsonapi_next submodule.

### 1.2.1

###### Features
* Mark all submodules as d11 ready after upgrade_status scan.


### 1.2.0

###### Features
* [[#14](https://gitlab.com/anvil-pub/anvil_toolkit/-/merge_requests/14)] Add the anvil_toolkit_jsonapi submodule.

### 1.1.1

###### Features
* [[#13](https://gitlab.com/anvil-pub/anvil_toolkit/-/merge_requests/13)] Add OverviewPageManager function to fetch a list of configured overview pages.

### 1.1.0

###### Features
* [[#9](https://gitlab.com/anvil-pub/anvil_toolkit/-/merge_requests/9)] Add anvil_toolkit_related_module.
* [[#10](https://gitlab.com/anvil-pub/anvil_toolkit/-/merge_requests/10)] Make anvil_toolkit_overview_pages easier extendable for multiple entity types (Running update_hooks is required. This will prefix all existing config with the entity type. For example: page becomes node.page, ...). Also removed some hardcoded defined content-types in the allowed values.
* [[#12](https://gitlab.com/anvil-pub/anvil_toolkit/-/merge_requests/12)] Add anvil_toolkit_ckeditor5_extras module for and a FilterParagraph filter to strip `<p>` tags as CKeditor5 does not allow this anymore.
###### Bugfixes
* [[#11](https://gitlab.com/anvil-pub/anvil_toolkit/-/merge_requests/11)] Fix wrong module name in anvil_toolkit hook causing anvil_toolkit.root menu item to not show up when anvil_toolkit_webforms isn't installed.
