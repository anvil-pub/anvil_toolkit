# Anvil toolkit
&nbsp;
<img alt="Drupal Logo" src="https://anvil.be/themes/custom/anvil/logo.png" height="48px">

## Info
This module contains reusable functionalities.
Activate the relevant module to enable desired features.

## List of module
- **[anvil_toolkit_ckeditor5_extras](modules%2Fanvil_toolkit_ckeditor5_extras)** <br>
  Provides extra ckeditor5 plugins.
- **[anvil_toolkit_developer_notes](modules%2Fanvil_toolkit_developer_notes)** <br>
  Add notes for developers on specific paths in the backoffice.
- **[anvil_toolkit_overview_pages](modules%2Fanvil_toolkit_overview_pages)** <br>
  Provides configuration to define overview pages.
- **[anvil_toolkit_suggestions](modules%2Fanvil_toolkit_suggestions)** <br>
  Generates various template suggestions.
- **[anvil_toolkit_social](modules%2Fanvil_toolkit_blocks)** <br>
  Configure social media links on '/admin/config/anvil/blocks/social-media'.
- **[anvil_toolkit_webforms](modules%2Fanvil_toolkit_webforms)** <br>
  Configure which webforms should be managed in configuration on 'admin/config/anvil/webforms/whitelist'. By default; webform config entities will not be overridden on config import.
- **[anvil_toolkit_related](modules%2Fanvil_toolkit_related)** <br>
  Adds a RelatedBlockBase to extend from which can be used to create related blocks with automatical content that can be manually overridden. Check the submodule README.md for more information.
- **[anvil_toolkit_jsonapi](modules%2Fanvil_toolkit_jsonapi)** <br>
  Provides various jsonapi plugins & enhancements.
