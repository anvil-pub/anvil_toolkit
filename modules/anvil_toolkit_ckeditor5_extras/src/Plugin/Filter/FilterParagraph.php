<?php

namespace Drupal\anvil_toolkit_ckeditor5_extras\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Annotation\Translation;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to strip <p> tags.
 *
 * @Filter(
 *   id = "anvil_filter_paragraph",
 *   title = @Translation("Strip <code>&lt;p&gt;</code> tags"),
 *   description = @Translation("Useful when you have markup that needs to be wrapped inside a different tag. <br> For example: &lt;h1&gt; {{ wysiwyg }} &lt;/h1&gt;."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   weight = -10
 * )
 */
class FilterParagraph extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {
    return new FilterProcessResult($this->filterParagraph($text));
  }

  /**
   * Strip <p> tags from the html.
   *
   * @param string $text
   *   The text to filter.
   *
   * @return string
   *   Filtered text.
   */
  public function filterParagraph(string $text): string {
    $dom = Html::load($text);
    $paragraphs = $dom->getElementsByTagName("p");
    if ($paragraphs->count() === 0) {
      return $text;
    }
    $innerHTML = '';
    foreach ($paragraphs as $p) {
      $innerHTML .= $this->extractInnerHtml($p);
    }
    return $innerHTML;
  }

  /**
   * Extract the inner HTML.
   *
   * @param \DOMElement $element
   *   The DOM element.
   *
   * @return string
   *   The extracted HTML.
   */
  protected function extractInnerHtml(\DOMElement $element): string {
    $innerHTML = '';
    $children = $element->childNodes;
    foreach ($children as $child) {
      $tmp_dom = Html::load('');
      $tmp_body = $tmp_dom->getElementsByTagName('body')->item(0);
      $tmp_body->appendChild($tmp_dom->importNode($child, TRUE));
      $innerHTML .= Html::serialize($tmp_dom);
    }

    return $innerHTML;
  }

}
