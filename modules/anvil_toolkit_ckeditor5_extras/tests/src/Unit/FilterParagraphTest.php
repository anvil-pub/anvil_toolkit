<?php

declare(strict_types=1);

namespace Drupal\Tests\anvil_toolkit_ckeditor5_extras\Unit;

use Drupal\anvil_toolkit_ckeditor5_extras\Plugin\Filter\FilterParagraph;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\anvil_toolkit_ckeditor5_extras\Plugin\Filter\FilterParagraph
 * @group filter
 */
class FilterParagraphTest extends UnitTestCase {

  /**
   * The filter.
   *
   * @var \Drupal\anvil_toolkit_ckeditor5_extras\Plugin\Filter\FilterParagraph
   */
  protected FilterParagraph $filter;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $configuration = [];

    $this->filter = new FilterParagraph($configuration, 'anvil_filter_paragraph', ['provider' => 'test']);
  }

  /**
   * @covers ::filterParagraph
   *
   * @dataProvider providerFilterParagraph
   *
   * @param string $html
   *   Input HTML.
   * @param string $expected
   *   The expected output string.
   */
  public function testFilterParagraph(string $html, string $expected): void {
    $this->assertSame($expected, $this->filter->filterParagraph($html));
  }

  /**
   * Provides data for testFilterParagraph.
   *
   * @return array
   *   An array of test data.
   */
  public function providerFilterParagraph(): array {
    return [
      ['<p>Example</p>', 'Example'],
      ['<p><span class="myclass">Example</span></p>', '<span class="myclass">Example</span>'],
      ['<p class="myclass">Example</p>', 'Example'],
      ['<p><p>Example</p></p>', 'Example'],
      ['<p><strong>Example</strong></p>', '<strong>Example</strong>'],
      ['<p><a href="https://anvil.be" class="button">Example</a></p>', '<a href="https://anvil.be" class="button">Example</a>'],
      ['<p>Highlight <mark class="highlight-blue">this</mark> text.</p>', 'Highlight <mark class="highlight-blue">this</mark> text.'],
      // This is broken HTML, so should return nothing.
      ['<p><div>Example</div></p>', '']
    ];
  }

}
