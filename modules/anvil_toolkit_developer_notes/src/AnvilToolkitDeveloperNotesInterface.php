<?php

namespace Drupal\anvil_toolkit_developer_notes;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an anvil toolkit developer notes entity type.
 */
interface AnvilToolkitDeveloperNotesInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
