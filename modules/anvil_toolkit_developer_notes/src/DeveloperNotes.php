<?php

namespace Drupal\anvil_toolkit_developer_notes;

use Drupal\anvil_toolkit_developer_notes\Utility\Admin;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * This class will query notes and display on the screen.
 */
class DeveloperNotes {

  /**
   * Name of content entity table.
   */
  const CONTENT_ENTITY_ID = 'anvil_toolkit_developer_notes';

  /**
   * Construct.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity typeManager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   Route match.
   * @param \Drupal\Core\Session\AccountProxyInterface $accountProxy
   *   Account proxy.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   Renderer.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected MessengerInterface $messenger,
    protected RouteMatchInterface $routeMatch,
    protected AccountProxyInterface $accountProxy,
    protected Renderer $renderer
  ) {}

  /**
   * Set all notes here by adding route checks for each message.
   */
  public function setAllDeveloperNotes(): void {
    // Developer notes only visible for the super admin.
    if (Admin::isSuperAdmin($this->accountProxy->getAccount())) {
      // List of notes.
      $notes = $this->getDeveloperNotes($this->routeMatch->getRouteName());
      // If results found.
      if (count($notes)) {
        // Set a note.
        foreach ($notes as $note) {
          $markup = [
            '#type' => 'markup',
            '#markup' => $note,
          ];
          $this->messenger->addMessage($this->renderer->render($markup));
        }
      }
    }

  }

  /**
   * Query all developer notes based on route.
   *
   * @param string $route_name
   *   Name of route.
   *
   * @return array
   *   Will return list notes.
   */
  protected function getDeveloperNotes(string $route_name): array {
    $notes = $this->entityTypeManager->getStorage(self::CONTENT_ENTITY_ID)
      ->loadByProperties(['route_name' => $route_name]);

    // List note values.
    return array_map(fn (AnvilToolkitDeveloperNotesInterface $note) => $note->get('note')->value, $notes);
  }

}
