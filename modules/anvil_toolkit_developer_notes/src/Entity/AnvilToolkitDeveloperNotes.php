<?php

namespace Drupal\anvil_toolkit_developer_notes\Entity;

use Drupal\anvil_toolkit_developer_notes\AnvilToolkitDeveloperNotesInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the anvil toolkit developer notes entity class.
 *
 * @ContentEntityType(
 *   id = "anvil_toolkit_developer_notes",
 *   label = @Translation("Anvil toolkit developer notes"),
 *   label_collection = @Translation("Anvil toolkit developer notes"),
 *   label_singular = @Translation("anvil toolkit developer notes"),
 *   label_plural = @Translation("anvil toolkit developer notes"),
 *   label_count = @PluralTranslation(
 *     singular = "@count anvil toolkit developer notes",
 *     plural = "@count anvil toolkit developer notes",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\anvil_toolkit_developer_notes\AnvilToolkitDeveloperNotesListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\anvil_toolkit_developer_notes\Form\AnvilToolkitDeveloperNotesForm",
 *       "edit" = "Drupal\anvil_toolkit_developer_notes\Form\AnvilToolkitDeveloperNotesForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "anvil_toolkit_developer_notes",
 *   admin_permission = "administer anvil toolkit developer notes",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/anvil-toolkit-developer-notes",
 *     "add-form" = "/anvil-toolkit-developer-notes/add",
 *     "canonical" = "/anvil-toolkit-developer-notes/{anvil_toolkit_developer_notes}",
 *     "edit-form" = "/anvil-toolkit-developer-notes/{anvil_toolkit_developer_notes}/edit",
 *     "delete-form" = "/anvil-toolkit-developer-notes/{anvil_toolkit_developer_notes}/delete",
 *   },
 *   field_ui_base_route = "entity.anvil_toolkit_developer_notes.settings",
 * )
 */
class AnvilToolkitDeveloperNotes extends ContentEntityBase implements AnvilToolkitDeveloperNotesInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);
    // Default fields.
    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the anvil toolkit developer notes was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the anvil toolkit developer notes was last edited.'));

    // Custom fields.
    // Route name.
    $fields['route_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Add route name'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 2,
      ])
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Note field.
    $fields['note'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Add a note'))
      ->setDisplayOptions('form', [
        'type' => 'plain_text',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'plain_text',
        'label' => 'above',
        'weight' => 3,
      ])
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
