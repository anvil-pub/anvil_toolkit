<?php

namespace Drupal\anvil_toolkit_developer_notes\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the anvil toolkit developer notes entity edit forms.
 */
class AnvilToolkitDeveloperNotesForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New anvil toolkit developer notes %label has been created.', $message_arguments));
        $this->logger('anvil_toolkit_developer_notes')->notice('Created new anvil toolkit developer notes %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The anvil toolkit developer notes %label has been updated.', $message_arguments));
        $this->logger('anvil_toolkit_developer_notes')->notice('Updated anvil toolkit developer notes %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.anvil_toolkit_developer_notes.canonical', ['anvil_toolkit_developer_notes' => $entity->id()]);

    return $result;
  }

}
