<?php

namespace Drupal\anvil_toolkit_developer_notes\Utility;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides helper methods for admin functionalities.
 */
class Admin {

  /**
   * Check whether an account is super admin.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   *   Returns allowed when user is the super admin.
   */
  public static function isSuperAdmin(AccountInterface $account) {
    if ((int) $account->id() === 1) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

}
