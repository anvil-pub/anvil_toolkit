<?php

namespace Drupal\anvil_toolkit_jsonapi\Plugin\Field;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Adds a raw computed field for the available translations.
 *
 * JSON:API does not show path aliases for translations or even which
 * translations exist for a node without manually checking them.
 * This field shows all existing translations & their path aliases.
 *
 * The most prominent use case is the language switcher.
 */
class AvailableTranslationsField extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue(): void {
    $item = NULL;

    $entity = $this->getEntity();
    $languageManager = \Drupal::languageManager();
    $pathAliasManager = \Drupal::service('path_alias.manager');

    $languages = $languageManager->getLanguages();
    $current_language = $languageManager->getCurrentLanguage();
    unset($languages[$current_language->getId()]);

    // Add every language path alias.
    foreach ($languages as $langcode => $language) {
      if ($entity->hasTranslation($langcode)) {
        $item[$langcode] = $pathAliasManager->getAliasByPath('/node/' . $entity->id(), $langcode);
      }
    }

    // BaseFieldDefinition doesn't allow arrays. So we Json::encode the item
    // here, and then we decode it with de JSON:API enhancer.
    $this->list[0] = $this->createItem(0, Json::encode($item));
  }

}
