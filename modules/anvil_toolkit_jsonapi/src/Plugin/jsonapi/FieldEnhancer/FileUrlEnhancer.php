<?php

namespace Drupal\anvil_toolkit_jsonapi\Plugin\jsonapi\FieldEnhancer;

use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\jsonapi_extras\Plugin\ResourceFieldEnhancerBase;
use Shaper\Util\Context;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Perform additional manipulations to the file url.
 *
 * @ResourceFieldEnhancer(
 *   id = "anvil_file_url",
 *   label = @Translation("[ANVIL] File url enhancer"),
 *   description = @Translation("Perform additional manipulations to the file url")
 * )
 */
class FileUrlEnhancer extends ResourceFieldEnhancerBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a new FileUrlEnhancer.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   The file url generator service.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    protected FileUrlGeneratorInterface $fileUrlGenerator,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('file_url_generator'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'absolute_url' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function doUndoTransform($data, Context $context) {
    $configuration = $this->getConfiguration();
    if ($configuration['absolute_url']) {
      $file_uri = $data['value'] ?? '';
      if (!empty($file_uri)) {
        $data['url'] = $this->fileUrlGenerator->generateAbsoluteString($file_uri);
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  protected function doTransform($data, Context $context) {
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getOutputJsonSchema(): array {
    return [
      'type' => 'object',
      'properties' => [
        'value' => ['type' => 'string'],
        'url' => ['type' => 'string'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSettingsForm(array $resource_field_info): array {
    $settings = empty($resource_field_info['enhancer']['settings'])
      ? $this->getConfiguration()
      : $resource_field_info['enhancer']['settings'];
    $form = parent::getSettingsForm($resource_field_info);
    $form['absolute_url'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Transform to absolute url'),
      '#default_value' => $settings['absolute_url'],
    ];

    return $form;
  }

}
