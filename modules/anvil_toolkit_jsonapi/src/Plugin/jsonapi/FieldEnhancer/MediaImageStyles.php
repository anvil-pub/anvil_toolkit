<?php

namespace Drupal\anvil_toolkit_jsonapi\Plugin\jsonapi\FieldEnhancer;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\consumer_image_styles\Plugin\jsonapi\FieldEnhancer\ImageStyles;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\image\ImageStyleInterface;
use Drupal\media\Entity\Media;
use Drupal\serialization\Normalizer\CacheableNormalizerInterface;
use Shaper\Util\Context;

/**
 * Perform additional manipulations on media image styles.
 *
 * @ResourceFieldEnhancer(
 *   id = "anvil_media_image_styles",
 *   label = @Translation("[ANVIL] Image Styles (Media(image) / image field)"),
 *   description = @Translation("Adds links for media & images with image styles
 *   applied to them. Make sure the media type is one that supports images.
 *   (not documents, videos, ...)")
 * )
 */
class MediaImageStyles extends ImageStyles implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  protected function doUndoTransform($data, Context $context) {
    // If it's not media we can just use parents logic.
    if ($data['type'] !== 'media--image') {
      return parent::doUndoTransform($data, $context);
    }

    $image_style_ids = $this->imageStylesForField();
    if (empty($image_style_ids)) {
      return $data;
    }

    // Load image style entities in bulk.
    try {
      $image_styles = $this->entityTypeManager
        ->getStorage('image_style')
        ->loadMultiple($image_style_ids);
    }
    catch (InvalidPluginDefinitionException $e) {
      $image_styles = [];
    }

    $uuid_key = $this->entityTypeManager->getDefinition('media')
      ->getKey('uuid');
    $entities = $this->entityTypeManager
      ->getStorage('media')
      ->loadByProperties([$uuid_key => $data['id']]);
    $media = reset($entities);

    // Set the image to continue.
    $entity = $media->field_media_image->entity ?? FALSE;

    // If the entity cannot be loaded or it's not an image, do not enhance it.
    if (!$entity || !$this->imageStylesProvider->entityIsImage($entity)) {
      return $data;
    }

    /** @var \Drupal\Core\Cache\CacheableMetadata $cacheableMetadata */
    $cacheableMetadata = $context->offsetGet(CacheableNormalizerInterface::SERIALIZATION_CONTEXT_CACHEABILITY)
      ->merge(CacheableMetadata::createFromObject($entity));
    /** @var \Drupal\file\Entity\File $entity */
    // If the entity is not viewable.
    $access = $entity->access('view', NULL, TRUE);
    if (!$access->isAllowed()) {
      return $data;
    }
    $uri = $entity->getFileUri();
    $links = array_map(
      function (ImageStyleInterface $image_style) use ($uri, $cacheableMetadata) {
        return $this->imageStylesProvider->buildDerivativeLink($uri, $image_style, $cacheableMetadata);
      },
      $image_styles
    );
    $meta = ['imageDerivatives' => ['links' => $links]];

    if ($media) {
      // Add media as cacheable dependency.
      $cacheableMetadata->addCacheableDependency($media);

      $this->alterMeta($meta, $media);
    }

    // Must explicitly re-set because CacheableMetadata::merge() clones.
    $context->offsetSet(
      CacheableNormalizerInterface::SERIALIZATION_CONTEXT_CACHEABILITY,
      $cacheableMetadata
    );

    return array_merge_recursive($data, ['meta' => $meta]);
  }

  /**
   * Do alters to the metadata.
   *
   * This function is specifically used for easy access to add media fields
   * and data to the JSON:API resource.
   *
   * @param array $meta
   *   The meta data to transform.
   * @param \Drupal\media\Entity\Media $media
   *   The media entity.
   */
  protected function alterMeta(array &$meta, Media $media): void {}

}
