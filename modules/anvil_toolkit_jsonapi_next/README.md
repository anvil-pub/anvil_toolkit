# Anvil Toolkit JSON:API Next

This module provides easy connection with a NextJs application.

### Features

- Automatically revalidate pages when saving nodes.
- Automatically revalidate entire application when saving configurable menu's
- Automatically revalidate entire application when configurable settings forms
- Dashboard for manually revalidating content/application.


### Installation

- Required
  - queue_unique
  - paragraphs
  - pathauto
  - node
- Recommended
  - entity_usage

For the queue_unique module we need to override the default queueFactory to use the
queue_unique class.

Add following to the **services.yml** file.

```
services:
  queue:
    class: Drupal\queue_unique\QueueFactory
      arguments: [ '@settings' ]
      calls:
        - [ setContainer, [ '@service_container' ] ]
```


### Revalidation

NEXTJS uses static site generation. All pages are generated on build. When saving content
in Drupal we need to regenerate content. This is called revalidation. This module provides the abiliy 
to revalidate the page in the front-end when saving the content in the back-end. Because the NextJs
applications are built using the Drupal decoupled router all connections are done using the path.

When a node is saved we revalidate the corresponing path in the front-end.

#### Entity usage

It is recommended to use the entity_usage module. By default this module will only revalidate the corersponding path
for each node when saving it. There are many cases where this is not enough. Entity references will become an immediate issue here.

For example, On node "A" there is a teaser of node "B". When editing node "B" and changing it's image, it 
will revalidate node "B" and thus the image will be changed on node "B". But node "A" has not been changed/saved, so
no revalidation occurred for node "A" and the teaser image of node "B" will still be the same.

Using entity usage we can find all usages of entities and revalidate all entity references to make sure
all correct content is revalidated.

All relationships are added to a revalidation queue to avoid too many requests.


#### Other

Using the settings it is possible to revalidate all content when updating menu's.
It's also possible to configure form_id's that should trigger revalidation for all content.

