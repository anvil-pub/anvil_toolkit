<?php

namespace Drupal\anvil_toolkit_jsonapi_next\Commands;

use Drupal\anvil_toolkit_jsonapi_next\Next;
use Drupal\Component\DependencyInjection\ContainerInterface;
use Drush\Commands\DrushCommands;

/**
 * Drush commands for revalidation.
 */
class RevalidateCommands extends DrushCommands {

  /**
   * The RevalidateCommands constructor.
   *
   * @param \Drupal\anvil_toolkit_jsonapi_next\Next $next
   *   The NextJS application.
   */
  public function __construct(protected Next $next) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get(Next::SERVICE),
    );
  }

  /**
   * Revalidate all content.
   *
   * @command revalidate:all
   * @aliases revall
   */
  public function revalidateAll(): void {
    try {
      $this->next->revalidate();

      $this->io()->success('Revalidation request sent successfully.');
    }
    catch (\Exception $e) {
      $this->io->error('Failed revalidation: ' . $e->getMessage());
    }
  }

  /**
   * Revalidate path.
   *
   * @param string $path
   *   The full path to revalidate (include language prefix if necessary).
   *
   * @usage revalidate:path /nl/events/my-event
   *
   * @command revalidate:path
   * @aliases revpath
   *
   */
  public function revalidatePath(string $path): void {
    try {
      $this->next->revalidate($path);

      $this->io()->success('Revalidation request sent successfully.');
    }
    catch (\Exception $e) {
      $this->io->error('Failed revalidation: ' . $e->getMessage());
    }
  }

}
