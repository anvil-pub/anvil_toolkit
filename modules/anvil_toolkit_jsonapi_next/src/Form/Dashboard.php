<?php

namespace Drupal\anvil_toolkit_jsonapi_next\Form;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\anvil_toolkit_jsonapi_next\Next;
use Drupal\anvil_toolkit_jsonapi_next\Plugin\QueueWorker\NextRevalidateQueue;
use Drupal\node\Entity\Node;
use Drupal\queue_unique\UniqueQueueDatabaseFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A dashboard form for NextJS management.
 */
class Dashboard extends FormBase {

  /**
   * The NextJs Dashboard Constructor.
   *
   * @param \Drupal\anvil_toolkit_jsonapi_next\Next $next
   *   The Next application.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter.
   * @param \Drupal\queue_unique\UniqueQueueDatabaseFactory $uniqueQueue
   *   The unique queue factory.
   */
  public function __construct(protected Next $next, protected LanguageManagerInterface $languageManager, protected DateFormatterInterface $dateFormatter, protected UniqueQueueDatabaseFactory $uniqueQueue) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(Next::SERVICE),
      $container->get('language_manager'),
      $container->get('date.formatter'),
      $container->get('queue_unique.database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'next_dashboard';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $app = $this->next->getApplication();

    $last_revalidation = $this->next->getLastRevalidationState();

    $last_revalidation = is_numeric($last_revalidation) ? $this->dateFormatter->formatTimeDiffSince($last_revalidation) : $this->t('Never');

    // Stop when we have no application.
    if (!$app) {
      $this->messenger()
        ->addError($this->t('No NextJs application found. Please contact support.'));
      return $form;
    }

    $form['front-end'] = [
      '#type' => 'markup',
      '#markup' => '<b>' . $this->t('Coupled NextJs application:') . ' </b><a target="_blank" href="' . $app . '">' . $app . '</a><br><br><br>',
      '#attributes' => [
        'class' => [],
      ],
    ];

    $form['revalidation'] = [
      '#type' => 'container',
    ];

    $form['revalidation']['last_revalidation'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<b>Last revalidation:</b> @last_revalidation', ['@last_revalidation' => $last_revalidation]) . '<br><br>',
    ];

    // Do some additional stuff in development mode.
    if ($this->next->inDevelopment()) {
      $this->messenger()->addWarning($this->t('Development Mode is enabled!'));

      $revalidation_results = \Drupal::database()
        ->query('SELECT * FROM {queue_unique} WHERE {name} = \'' . NextRevalidateQueue::QUEUE . '\'')
        ->fetchAll();

      $to_revalidate = [];
      foreach ($revalidation_results as $key => $revalidation_result) {
        $data = unserialize($revalidation_result->data);
        $path = $data['path'];

        if ($path === '') {
          $path = $this->t('All');
        }

        $to_revalidate[] = ['#markup' => $path];
      }

      $form['revalidation_queue'] = [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#title' => $this->t('Revalidation queue: '),
        '#items' => $to_revalidate ? $to_revalidate : ['#markup' => 'Queue is empty.'],
      ];
    }

    $form['revalidation']['slug'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#title' => $this->t('Revalidate only a single page'),
      '#placeholder' => $this->t('Page (1)'),
      '#default_value' => '',
    ];

    $form['revalidation']['warning'] = [
      '#type' => 'markup',
      '#markup' => '<br>' . $this->t('<b>This will revalidate ALL pages</b> (except when using the single page option). This can take sometime.') . '<br>',
    ];

    $form['revalidation']['revalidate'] = [
      '#type' => 'submit',
      '#value' => $this->t('Revalidate'),
      '#attributes' => [
        'class' => ['button', 'button--danger', 'button--primary'],
      ],
    ];

    $form['revalidation']['help'] = [
      '#type' => 'markup',
      '#markup' => '<br><small>' . $this->t('NextJs uses static page generation. This means that all pages are generated when building the application. With revalidation we "re-generate" pages so they are updated to the latest version. Most revalidation will happen automatically but if content is somehow not updating automatically, you can use this button to force a complete revalidation or revalidate a single page.') . '</small>',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    if ($form_state->getValue('slug')) {
      $nid = $form_state->getValue('slug');

      $node = Node::load($nid);

      if ($node instanceof Node) {
        $this->next->revalidateEntity($node);
      }
    }
    else {
      // We can immediately clear the revalidate queue because we are revalidating
      // everything here now anyway.
      $this->uniqueQueue->get(NextRevalidateQueue::QUEUE)->deleteQueue();

      // Revalidate all.
      $this->next->revalidate();
    }
  }

}
