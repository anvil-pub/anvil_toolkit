<?php

namespace Drupal\anvil_toolkit_jsonapi_next\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\system\Entity\Menu;

/**
 * Configure Next settings.
 */
class SettingsForm extends ConfigFormBase {
  const SETTINGS = 'anvil_toolkit_jsonapi_next.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'anvil_toolkit_jsonapi_next_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $field_type = NULL): array {
    $config = $this->configFactory->get(static::SETTINGS);

    $form['#tree'] = TRUE;

    $form['base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The base url'),
      '#default_value' => $config->get('base_url'),
      '#description' => $this->t('Front-end url without trailing "/". E.g. https://kmska.be'),
    ];

    $form['revalidation'] = [
      '#type' => 'details',
      '#title' => $this->t('Revalidation'),
      '#open' => TRUE,
    ];

    $form['revalidation']['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The revalidation path'),
      '#default_value' => $config->get('revalidation.path'),
      '#description' => $this->t('The endpoint for revalidation. E.g. /api/revalidate'),
    ];

    $form['revalidation']['secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The revalidation secret'),
      '#default_value' => $config->get('revalidation.secret'),
      '#description' => $this->t('The secret for revalidation.'),
    ];

    $form['revalidation']['dev'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Development mode'),
      '#default_value' => $config->get('revalidation.dev'),
      '#description' => $this->t('Enables extra debugging, disable SSL verification, ...'),
    ];

    $node_types = NodeType::loadMultiple();
    $bundles = [];

    foreach ($node_types as $key => $node_type) {
      $bundles[$key] = $node_type->label();
    }

    $form['revalidation']['node']['bundles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content types'),
      '#options' => $bundles,
      '#description' => $this->t('Enable onSave revalidation for following types.'),
      '#default_value' => $config->get('revalidation.node.bundles'),
    ];

    if (\Drupal::moduleHandler()->moduleExists('entity_usage')) {
      $form['revalidation']['cross_check'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Cross check entity references (requires entity_usage)'),
        '#description' => $this->t('Enable checking for relationships when saving content. This will automatically cross check entity_usages for the node bundles that are selected in the checkboxes above.'),
        '#default_value' => $config->get('revalidation.cross_check'),
      ];
    }

    $menus = Menu::loadMultiple();
    $bundles = [];
    foreach ($menus as $key => $node_type) {
      $bundles[$key] = $node_type->label();
    }

    $form['revalidation']['menu']['types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Revalidate entire application when following menu is updated.'),
      '#options' => $bundles,
      '#description' => $this->t('This will be added to a queue to avoid too much revalidation calls.'),
      '#default_value' => $config->get('revalidation.menu.types'),
    ];

    $form['revalidation']['forms'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Revalidate entire application when following form is updated.'),
      '#description' => $this->t('One new line for each form id. This will be added to a queue to avoid too much revalidation calls.'),
      '#default_value' => $config->get('revalidation.forms'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $config = $this->config(static::SETTINGS);
    $values = $form_state->cleanValues()->getValues();

    unset($values['actions']);

    foreach ($values as $key => $formValue) {
      $config->set($key, $formValue)->save();
    }
  }

}
