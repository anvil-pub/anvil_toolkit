<?php

namespace Drupal\anvil_toolkit_jsonapi_next;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Drupal\anvil_toolkit_jsonapi_next\Form\SettingsForm;
use Drupal\anvil_toolkit_jsonapi_next\Plugin\QueueWorker\NextRevalidateQueue;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\queue_unique\UniqueQueueDatabaseFactory;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;

/**
 * The Next service.
 */
class Next {

  const SERVICE = 'anvil_toolkit_jsonapi_next.next';

  const LAST_REVALIDATE_STATE = 'last_revalidation';

  /**
   * The application url.
   *
   * @var string
   */
  protected string $application;

  /**
   * The revalidate url.
   *
   * @var string
   */
  protected string $revalidatePath;

  /**
   * The revalidate secret.
   *
   * @var string
   */
  protected string $secret;

  /**
   * List of bundles to revalidate.
   *
   * @var array
   */
  protected array $bundlesToRevalidate;

  /**
   * Check references.
   *
   * @var bool
   */
  protected bool $doCrossCheckReferences;

  /**
   * Whether development Mode is enabled.
   *
   * @var bool
   */
  protected bool $inDevelopment;

  /**
   * Next constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The http client.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\queue_unique\UniqueQueueDatabaseFactory $uniqueQueue
   *   The unique queue.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match.
   */
  public function __construct(protected ConfigFactoryInterface $configFactory, protected ClientInterface $httpClient, protected LoggerInterface $logger, protected StateInterface $state, protected MessengerInterface $messenger, protected ModuleHandlerInterface $moduleHandler, protected UniqueQueueDatabaseFactory $uniqueQueue, protected LanguageManagerInterface $languageManager, protected RouteMatchInterface $routeMatch) {
    $settings = $this->configFactory->get(SettingsForm::SETTINGS);
    $this->application = $settings->get('base_url') ?? '';
    $this->secret = $settings->get('revalidation.secret') ?? '';
    $this->revalidatePath = $settings->get('revalidation.path') ?? '';
    $this->bundlesToRevalidate = $settings->get('revalidation.node.bundles') ? array_values($settings->get('revalidation.node.bundles')) : [];
    $this->doCrossCheckReferences = $settings->get('revalidation.cross_check') ?? FALSE;
    $this->inDevelopment = $settings->get('revalidation.dev') ?? FALSE;
  }

  /**
   * Revalidate content in the NextJs application.
   *
   * The NextJs application has a webhook /api/revalidate
   * which needs a secret and an optional slug. When using
   * The slug it will revalidate that page. Without the slug
   * it will revalidate ALL pages.
   *
   * Revalidation is similar to clearing caches.
   *
   * @param string $slug
   *   (optional) Specific single slug to revalidate.
   */
  public function revalidate(string $slug = ''): void {
    try {
      $url = Url::fromUri($this->getApplication() . $this->getRevalidateUrl(), []);

      $query = [
        'secret' => $this->getSecret(),
      ];

      // No slug will revalidate ALL pages.
      if (!empty($slug)) {
        $query['slug'] = $slug;
      }

      $url->setOption('query', $query);

      $response = $this->httpClient->request('GET', $url->toString(), [
        'verify' => !($this->inDevelopment()),
        'debug' => $this->inDevelopment(),
      ]);

      if ($response->getStatusCode() === 200) {
        // We only want revalidation state when we revalidate ALL pages.
        if (!$slug) {
          $this->messenger->addStatus('Application successfully started revalidation for all pages.');
          $timestamp = \Drupal::time()->getCurrentTime();
          $this->setLastRevalidationState($timestamp);
        }
        else {
          $this->messenger->addStatus('Application successfully started revalidation for: ' . $slug);
        }
      }
      else {
        $this->messenger->addError('Application could not start revalidation. Content on the website has not been updated with the latest changes.');
        $this->logger->error('Application failed revalidation: ' . $response->getReasonPhrase());
      }
    }
    catch (GuzzleException $e) {
      $this->messenger->addError('Application could not start revalidation. Content on the website has not been updated with the latest changes. Please try again later or contact support.');
      $this->logger->error('Revalidation failed: ' . $e->getMessage());
    }
  }

  /**
   * Revalidate an entity.
   *
   * Revalidate a specific given entity. This will also add
   * the entity translations to the revalidate queue.
   *
   * Optionally you can include crosschecks using entity_usage
   * to revalidate all entities the given entity is being referenced from.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $action
   *   (Optional) Some actions require specific functionalities (delete).
   * @param bool $include_crosscheck
   *   Cross-check related references.
   */
  public function revalidateEntity(EntityInterface $entity, string $action = '', bool $include_crosscheck = FALSE): void {
    $settings = $this->configFactory->get(SettingsForm::SETTINGS);
    switch ($entity->getEntityTypeId()) {
      case 'node':

        // Only revalidate checked bundles in config.
        if (!in_array($entity->bundle(), $this->getBundlesToRevalidate())) {
          return;
        }

        $path = $entity->hasLinkTemplate('canonical') ? $entity->toUrl()
          ->toString(TRUE)
          ->getGeneratedUrl() : NULL;
        if (!$path) {
          $this->messenger->addError('No path found for "' . $entity->label() . '".');
          $this->logger->error('Failed to find a path to revalidate for node: ' . $entity->id());
        }

        // When deleting we use pre-delete hook instead of delete
        // because otherwise we lose important information (path-alias,
        // entity_usages, ...). But we also want to make sure we don't
        // send the revalidate request to early (before deletion actually
        // happened), so we add it to the queue instead.
        if ($action === 'delete') {
          $this->addToRevalidateQueue($path);
        }
        else {
          $this->revalidate($path);
        }

        $this->revalidateEntityTranslations($entity);

        if ($this->doCrossCheck() && $include_crosscheck && $this->moduleHandler->moduleExists('entity_usage')) {
          $this->crossCheckEntityReferences($entity);
        }

        break;

      case 'menu_link_content':
        $menu_name = $entity->getMenuName();

        $route_name = $this->routeMatch->getRouteName();

        // When saving a node it can trigger a menu save because of the menu
        // sidebar on node edit forms. We do not want to revalidate the entire
        // app everytime a node is saved. We check whether the menu items on a
        // node edit form is changed. We only check this for node edit forms
        // because some menu's can have extra fields which should still
        // revalidate the application when saving it.
        if (in_array($route_name, ['entity.node.edit_form', 'node.add'])) {

          $og_label = $entity->original->label();
          $label = $entity->label();

          $og_link = $entity->original->get('link');
          $link = $entity->get('link');

          if (($og_label === $label) && ($link->first()->getString() === $og_link->first()->getString())) {
            return;
          }
        }

        if (in_array($menu_name, array_values($settings->get('revalidation.menu.types')))) {
          $this->addToRevalidateQueue('', TRUE);
        }

        break;

      case 'menu':
        $menu_name = $entity->bundle();
        if (in_array($menu_name, array_values($settings->get('revalidation.menu.types')))) {
          $this->addToRevalidateQueue('', TRUE);
        }
        break;
    }
  }

  /**
   * Cross-check entity references for revalidation.
   *
   * Requires entity_usage module.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to cross-check.
   */
  protected function crossCheckEntityReferences(EntityInterface $entity): void {
    /** @var \Drupal\entity_usage\EntityUsage $entity_usage */
    $entity_usage = \Drupal::service('entity_usage.usage');

    // Get all sources that use the current entity as target reference.
    $sources = $entity_usage->listSources($entity);
    $original_id = $entity->id();
    foreach ($sources as $source_type => $source_group) {
      // We don't need usages that have the current entity as usage.
      // These are all paragraphs or content that are available on the current
      // entity. So we can safely remove them.
      if (isset($source_group[$original_id])) {
        unset($sources[$source_type][$original_id]);
      }

      foreach ($source_group as $eid => $source) {
        $relation = NULL;

        // Do entity type specific logic.
        switch ($source_type) {
          case 'node':

            $relation = Node::load($eid);
            break;

          case 'paragraph':
            $relation = Paragraph::load($eid);

            if ($relation instanceof Paragraph) {
              // Keep getting parent entity until we're out of
              // the nested paragraph loop.
              while ($relation instanceof Paragraph) {
                $relation = $relation->getParentEntity();
              }
            }
            break;
        }

        // Only revalidate checked bundles in config.
        if ($relation instanceof Node && !in_array($relation->bundle(), $this->getBundlesToRevalidate())) {
          continue;
        }

        if ($relation->hasLinkTemplate('canonical')) {
          $this->addToRevalidateQueue($relation->toUrl()
            ->toString(TRUE)
            ->getGeneratedUrl());

          // @todo: Should we revalidate translations of entities that reference the current entity?
          $this->revalidateEntityTranslations($relation);
        }
      }
    }
  }

  /**
   * Revalidate entity translations.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to revalidate translations from.
   */
  protected function revalidateEntityTranslations(EntityInterface $entity): void {
    // Revalidate translations.
    $languages = $this->languageManager->getLanguages();
    unset($languages[$entity->language()->getId()]);
    foreach ($languages as $language) {
      $langcode = $language->getId();
      if ($entity->hasTranslation($language->getId())) {
        $translation = $entity->getTranslation($langcode);
        $this->addToRevalidateQueue($translation->toUrl()
          ->toString(TRUE)
          ->getGeneratedUrl(), FALSE);
      }
    }
  }

  /**
   * Add item to the revalidation queue.
   *
   * @param string $path
   *   The path to revalidate.
   * @param bool $delete_queue
   *   TRUE if the queue should be deleted first.
   */
  public function addToRevalidateQueue(string $path = '', bool $delete_queue = FALSE): void {
    $queue = $this->uniqueQueue->get(NextRevalidateQueue::QUEUE);

    if ($delete_queue) {
      $queue->deleteQueue();
    }

    $queue->createItem([
      'path' => $path,
    ]);
  }

  /**
   * Get the revalidation secret.
   *
   * @return string
   *   The revalidation secret.
   */
  protected function getSecret(): string {
    return $this->secret;
  }

  /**
   * Get the application url.
   *
   * @return string
   *   The application url.
   */
  public function getApplication(): string {
    return $this->application;
  }

  /**
   * Get the revalidation url.
   *
   * @return string
   *   The revalidation url.
   */
  public function getRevalidateUrl(): string {
    return $this->revalidatePath;
  }

  /**
   * Get bundles to revalidate.
   *
   * @return array
   *   List of bundles
   */
  public function getBundlesToRevalidate(): array {
    return $this->bundlesToRevalidate;
  }

  /**
   * Whether to cross-check references.
   *
   * @return bool
   *   TRUE if references should be checked.
   */
  public function doCrossCheck(): bool {
    return $this->doCrossCheckReferences;
  }

  /**
   * Get last revalidation state.
   *
   * @return mixed
   *   The revalidation state.
   */
  public function getLastRevalidationState(): mixed {
    return $this->state->get(static::LAST_REVALIDATE_STATE);
  }

  /**
   * Set the last revalidation state.
   *
   * @param int $timestamp
   *   Timestamp of the revalidation.
   */
  public function setLastRevalidationState(int $timestamp): void {
    $this->state->set(static::LAST_REVALIDATE_STATE, $timestamp);
  }

  /**
   * Whether or not development mode is enabled.
   *
   * @return bool
   *   TRUE if in development mode.
   */
  public function inDevelopment(): bool {
    return $this->inDevelopment;
  }

}
