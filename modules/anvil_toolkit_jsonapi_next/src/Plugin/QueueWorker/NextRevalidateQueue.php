<?php

namespace Drupal\anvil_toolkit_jsonapi_next\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\anvil_toolkit_jsonapi_next\Next;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Next revalidation Queue Worker.
 *
 * @QueueWorker(
 *   id = "queue_unique/next_revalidate_queue",
 *   title = @Translation("Revalidate Next Queue"),
 *   cron = {"time" = 15}
 * )
 */
class NextRevalidateQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  const QUEUE = 'next_revalidate_queue';

  /**
   * Main constructor.
   *
   * @param array $configuration
   *   Configuration array.
   * @param mixed $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\anvil_toolkit_jsonapi_next\Next $next
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected Next $next,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * Used to grab functionality from the container.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   * @param array $configuration
   *   Configuration array.
   * @param mixed $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get(Next::SERVICE),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    if (isset($data['path'])) {
      $this->next->revalidate($data['path']);
    }
  }

}
