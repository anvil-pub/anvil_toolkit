<?php

namespace Drupal\anvil_toolkit_overview_pages\Form;

use Drupal\anvil_toolkit_overview_pages\OverviewPagesManager;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form to configure overview pages per bundle.
 *
 * @package Drupal\anvil_toolkit_overview_pages\Form
 */
class OverviewPagesSettingsForm extends ConfigFormBase {

  const CONFIG_NAME = 'anvil_toolkit_overview_pages.settings';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The overview page manager.
   *
   * @var \Drupal\anvil_toolkit_overview_pages\OverviewPagesManager
   */
  protected OverviewPagesManager $overviewPageManager;

  /**
   * OverviewPagesSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\anvil_toolkit_overview_pages\OverviewPagesManager $overview_page_manager
   *   The overview page manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, OverviewPagesManager $overview_page_manager) {
    parent::__construct($config_factory);

    $this->entityTypeManager = $entity_type_manager;
    $this->overviewPageManager = $overview_page_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('anvil_toolkit_overview_pages.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'anvil_toolkit_overview_pages_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::CONFIG_NAME,
    ];
  }

  /**
   * List the various bundles that can have an overview page.
   *
   * @return array
   *   Array with entity as key, and the allowed bundles as values.
   */
  public function getOverviewEntityBundles(): array {
    return [
      'node' => node_type_get_names(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    if ($entity_bundles = $this->getOverviewEntityBundles()) {
      $config = $this->config(static::CONFIG_NAME);

      foreach ($entity_bundles as $entity_type => $bundles) {
        $form[$entity_type] = [
          '#type' => 'details',
          '#title' => Unicode::ucfirst($entity_type),
          '#open' => TRUE,
        ];
        foreach ($bundles as $bundle => $label) {
          $form[$entity_type][$bundle]['target_id'] = [
            '#title' => $this->t('@bundle_label (machine name: @bundle)', [
              '@bundle_label' => Unicode::ucfirst($label),
              '@bundle' => $bundle,
            ]),
            '#type' => 'entity_autocomplete',
            '#target_type' => 'node',
            '#description' => $this->t("Choose a @entity_type to represent the overview page of all <u>@type</u> Nodes.", [
              '@type' => Unicode::ucfirst($label),
              '@entity_type' => $entity_type,
            ]),
          ];

          if ($nid = $config->get($entity_type . '.' . $bundle)) {
            if ($node = $this->entityTypeManager->getStorage('node')->load($nid)) {
              $form[$entity_type][$bundle]['target_id']['#default_value'] = $node;
              $form[$entity_type][$bundle]['target_id']['#description'] .= '<br />' . $this->t('<strong>Currently set to <a href="@target" target="_blank">@label</a></strong>', [
                '@target' => $this->overviewPageManager->getOverviewPageUrl($bundle, $entity_type)->toString(),
                '@label' => $node->label(),
              ]);
            }
          }
        }
      }
    }

    $form['#tree'] = TRUE;

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $entity_bundles = $this->getOverviewEntityBundles();

    if (!empty($entity_bundles)) {
      $values = $form_state->getValues();

      foreach ($entity_bundles as $entity_type => $bundles) {
        foreach ($bundles as $bundle => $label) {
          if ($nid = $values[$entity_type][$bundle]['target_id']) {
            $this->config(static::CONFIG_NAME)
              ->set($entity_type . '.' . $bundle, $nid)
              ->save();
          }
          else {
            $this->config(static::CONFIG_NAME)
              ->set($entity_type . '.' . $bundle, NULL)
              ->save();
          }
        }
      }
    }
  }

}
