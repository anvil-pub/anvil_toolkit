<?php

namespace Drupal\anvil_toolkit_overview_pages;

use Drupal\anvil_toolkit_overview_pages\Form\OverviewPagesSettingsForm;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A service to get overview pages of Node bundles.
 *
 * @package Drupal\anvil_toolkit_overview_pages
 */
class OverviewPagesManager implements ContainerInjectionInterface {

  /**
   * The configuration.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected Config|ImmutableConfig $config;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected LanguageManager $languageManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * OverviewPagesManager constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Language\LanguageManager $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LanguageManager $language_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->config = $config_factory->get(OverviewPagesSettingsForm::CONFIG_NAME);
    $this->languageManager = $language_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('language_manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Get the Url to an overview page of a given Node bundle.
   *
   * @param string $bundle
   *   The bundle for which get the overview page Url.
   * @param string $entity_type
   *   The bundle for which get the overview page Url.
   * @param string|null $langcode
   *   The lang code in which to get the overview page Url.
   *
   * @return \Drupal\Core\Url|null
   *   The Url to the overview page, or NULL if none was found.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getOverviewPageUrl(string $bundle, string $entity_type = 'node', string $langcode = NULL): ?Url {
    $url = NULL;

    if ($nid = $this->config->get($entity_type . '.' . $bundle)) {
      // Make sure the configured Node still exists.
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      if ($entity = $this->entityTypeManager->getStorage('node')->load($nid)) {
        $languages = $this->languageManager->getLanguages();
        $language = ($langcode && array_key_exists($langcode, $languages)) ? $languages[$langcode] : $this->languageManager->getCurrentLanguage();

        $route_params['node'] = $entity->id();
        $route_params = (!empty($params)) ? $route_params += $params : $route_params;

        $url = Url::fromRoute(
          'entity.node.canonical', $route_params,
          [
            'language' => $language,
            'attributes' => [
              'target' => '_blank',
            ],
          ]
        );
      }
    }

    return $url;
  }

  /**
   * Get a list of configured overview pages, filtered by entity type.
   *
   * @param string $entity_type
   *   The entity type to get overview pages for.
   *
   * @return array
   *   An array of overview entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getOverviewPages(string $entity_type = 'node'): array {
    $overview_pages = $this->config->get($entity_type) ?? [];
    return array_filter(array_map(function ($entity_id) use ($entity_type) {
      return $entity_id ? $this->entityTypeManager->getStorage($entity_type)->load($entity_id) : NULL;
    }, $overview_pages));
  }

}
