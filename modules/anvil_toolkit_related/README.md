### How it works

This module provides a RelatedBlockBase to extend from and create 
your own RelatedBlock. It automatically generates related entities based
on (optional) taxonomy terms to filter the content with and an option
to manually override this content based on a field.

### Requirements

1. Make sure your entity_type has a related field. Something like field_nodes_related or field_terms_related, ...
2. (Optional) Taxonomy term field(s) which will be used as filtering for the related content. Something like field_tags, field_services, field_category, ... If there is no taxonomy term to filter on and just want to show other nodes you can leave getTermReferenceFieldNames() empty.

### Example usage

In the following example we have a content-type "case" where we
want to show related cases on a page based on whether they have similar sectors, technologies and services.
We want them to be shown in a info_card view mode.

```
<?php

namespace Drupal\anvil_case\Plugin\Block;

use Drupal\anvil_toolkit_related\Plugin\Block\RelatedBlockBase;

/**
 * Provides a Block to display related cases content.
 *
 * @Block(
 *   id = "anvil_case_related_block",
 *   admin_label = @Translation("Related cases"),
 *   category = @Translation("ANVIL"),
 * )
 */
class RelatedCasesBlock extends RelatedBlockBase {

  /**
   * {@inheritdoc}
   */
  protected function getRelatedFieldName(): string {
    return 'field_nodes_related';
  }

  /**
   * {@inheritdoc}
   */
  protected function getTermReferenceFieldNames(): array {
    return ['field_sectors', 'field_technologies', 'field_services'];
  }

  /**
   * {@inheritdoc}
   */
  protected function getBundles(): array {
    return ['case'];
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityViewMode(): string {
    return 'info_card';
  }

  /**
   * {@inheritdoc}
   */
  protected function getThemeHook(): string {
    return 'anvil_case_related_block';
  }

}
```