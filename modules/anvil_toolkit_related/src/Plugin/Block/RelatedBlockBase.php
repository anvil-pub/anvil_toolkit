<?php

namespace Drupal\anvil_toolkit_related\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityViewBuilderInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base class for related entity blocks.
 */
abstract class RelatedBlockBase extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The current entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface|null
   *   The current entity.
   */
  protected ?EntityInterface $currentEntity;

  /**
   * The entity view builder.
   *
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   *   The entity view builder service.
   */
  protected EntityViewBuilderInterface $entityViewBuilder;

  /**
   * The entity storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   *   The entity storage service.
   */
  protected EntityStorageInterface $entityStorage;

  /**
   * Constructs a RelatedBlockBase.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRouteMatch
   *   Default object for current_route_match service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Provides an interface for entity type managers.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected CurrentRouteMatch $currentRouteMatch,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentEntity = $this->currentRouteMatch->getParameter($this->getEntityType());

    // If entity type is node but we did not find a matching route
    // we check for node_preview.
    if (!$this->currentEntity && $this->getEntityType() === 'node' && $node_preview = $this->currentRouteMatch->getParameter('node_preview')) {
      $this->currentEntity = $node_preview;
    }

    $this->entityViewBuilder = $this->entityTypeManager->getViewBuilder($this->getEntityType());
    $this->entityStorage = $this->entityTypeManager->getStorage($this->getEntityType());
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): ContainerFactoryPluginInterface {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {

    if (!$this->currentEntity instanceof EntityInterface) {
      return [];
    }

    $cache_list = $this->currentEntity->getEntityType()->getListCacheTags();
    $ids = $this->getManuallyReferencedEntityIds();
    $related_entities = $this->entityStorage->loadMultiple(array_merge(
      $ids,
      $this->getRelatedEntityIdsByTermIds($ids),
    ));

    if (empty($related_entities)) {
      return [
        '#cache' => [
          'tags' => array_map(function (string $bundles, string $list) {
            return "$list:$bundles";
          }, $this->getBundles(), $cache_list),
        ],
      ];
    }

    return [
      '#theme' => $this->getThemeHook(),
      '#title' => $this->getTitle(),
      '#items' => array_map(function (EntityInterface $entity) {
        return $this->entityViewBuilder->view($entity, $this->getEntityViewMode());
      }, $related_entities),
    ];
  }

  /**
   * Specify the bundles necessary for querying related entities.
   *
   * @return array
   *   A list of entity bundles.
   */
  abstract protected function getBundles(): array;

  /**
   * Get the view mode to render the related entities in.
   *
   * @return string
   *   The entity view mode.
   */
  abstract protected function getEntityViewMode(): string;

  /**
   * The field name that holds te related entities.
   *
   * For example: field_related, field_nodes, ...
   *
   * @return string
   *   The field name.
   */
  abstract protected function getRelatedFieldName(): string;

  /**
   * Term reference fields on the (parent) entity used to query related content.
   *
   * Leave empty if there is no taxonomy field to get related entities for.
   * It will then take most recent entities.
   *
   * @return array
   *   The taxonomy term reference field names.
   */
  abstract protected function getTermReferenceFieldNames(): array;

  /**
   * List entity ids that were manually provided by the user.
   *
   * @return array
   *   List entity ids.
   */
  protected function getManuallyReferencedEntityIds(): array {
    $field_name = $this->getRelatedFieldName();
    if ($this->currentEntity->hasField($field_name)) {
      $published_entities = array_filter($this->currentEntity->{$field_name}->referencedEntities(), function (EntityInterface $entity) {
        return $entity->isPublished();
      });

      return array_map(function (EntityInterface $entity) {
        return $entity->id();
      }, $published_entities);
    }

    return [];
  }

  /**
   * Get related entity ids based on term ids provided by the user.
   *
   * @param array $exclude_ids
   *   List of manually inserted entity_ids by the user.
   *
   * @return array
   *   List entity ids.
   */
  protected function getRelatedEntityIdsByTermIds(array $exclude_ids): array {
    $query = $this->entityStorage->getQuery();

    $entity_type = $this->currentEntity->getEntityType();

    // Filter on the various requested node 'types' or taxonomy term 'vid'.
    if ($bundles = $this->getBundles()) {
      $query->condition($entity_type->getKey('bundle'), $bundles, 'IN');
    }

    // Avoid current and manually inserted entities.
    $exclude_ids[] = $this->currentEntity->id();
    $query->condition($entity_type->getKey('id'), $exclude_ids, 'NOT IN');

    if (!empty($this->getTermReferenceFieldNames())) {
      $or_group = $query->orConditionGroup();
      // Check the configured field values on the entity.
      foreach ($this->getTermReferenceFieldNames() as $field_name) {
        if ($this->currentEntity->hasField($field_name) && $this->currentEntity->get($field_name)->count()) {
          $target_ids = [];
          // This logic allows for the possibility of having multiple target IDs.
          foreach ($this->currentEntity->get($field_name)->getValue() as $value) {
            // Add conditions.
            $target_ids[] = $value['target_id'];
          }
          // Add condition only when ids exists.
          if (count($target_ids)) {
            $or_group->condition($field_name, $target_ids, 'IN');
          }
        }
      }
      // Add condition only if or group has conditions.
      if ($or_group->count()) {
        $query->condition($or_group);
      }
    }

    $query->range(0, $this->calculateRemainingEntityCount());
    $this->applyExtraConditions($query);
    $this->applySorting($query);

    // Filter on published entities.
    $query->condition('status', 1);

    $query->accessCheck();
    return $query->execute();
  }

  /**
   * Apply extra conditions to the Query.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The Query on which to apply extra conditions.
   */
  protected function applyExtraConditions(QueryInterface &$query): void {}

  /**
   * Apply sorting to the Query.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The Query on which to apply sorting.
   */
  protected function applySorting(QueryInterface &$query): void {
    $query->sort('changed', 'DESC');
  }

  /**
   * The number of entites that must be automatically generated.
   *
   * @return int
   *   The number of entities to auto-generate.
   */
  protected function calculateRemainingEntityCount(): int {
    $field_name = $this->getRelatedFieldName();

    $count = 0;
    if ($this->currentEntity->hasField($field_name)) {
      $count = $this->currentEntity->{$field_name}->count();
    }

    return $this->limit() - $count;
  }

  /**
   * Get the title for the block.
   *
   * @return string
   *   The block title (Defaults to the block label)
   */
  protected function getTitle(): string {
    return $this->label();
  }

  /**
   * Get the entity type for the related entities.
   *
   * Defaults to node as it is the most common use case.
   *
   * @return string
   *   The entity type.
   */
  protected function getEntityType(): string {
    return 'node';
  }

  /**
   * Amount of related entities that should be shown.
   *
   * Defaults to 3 as it is the most common use case.
   *
   * @return int
   *   Number of entities.
   */
  protected function limit(): int {
    return 3;
  }

  /**
   * Get the theme hook to use.
   *
   * @return string
   *   The theme hook.
   */
  protected function getThemeHook(): string {
    return 'anvil_toolkit_related_block';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    $contexts = parent::getCacheContexts();
    $contexts[] = 'url';
    return $contexts;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    if (!$this->currentEntity instanceof EntityInterface) {
      return parent::getCacheTags();
    }

    $cache_list = $this->currentEntity->getEntityType()->getListCacheTags();
    return Cache::mergeTags(parent::getCacheTags(), array_map(function (string $bundle, string $list) {
      return "$list:$bundle";
    }, $this->getBundles(), $cache_list));
  }

}
