<?php

namespace Drupal\anvil_toolkit_social\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Contains logic to define social media channel links.
 *
 * @package Drupal\anvil_toolkit_social\Form
 */
class SocialLinksForm extends ConfigFormBase {

  const CONFIG_NAME = 'anvil_toolkit_social.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'anvil_toolkit_social_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(static::CONFIG_NAME);
    $channels = $config->get('channels') ?? [];

    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Use this form to add and/or remove the website social media channels.'),
    ];

    // Get the number of configured channels, either from the form state or the
    // current configuration.
    $num_lines = $form_state->get('channels') ?? (empty($channels) ? 1 : count($channels));

    // Save the current number of channels to the form state.
    $form_state->set('channels', $num_lines);

    // Get the removed channels, either from the form state or - if not set -
    // fallback to an empty array.
    $removed_fields = $form_state->get('removed_fields') ?? [];

    // Save the number of removed channels to the form state.
    $form_state->set('removed_fields', $removed_fields);

    $form['#tree'] = TRUE;

    $form['channels'] = [
      '#type' => 'fieldset',
      '#prefix' => '<div id="channels-wrapper">',
      '#suffix' => '</div>',
    ];

    for ($i = 0; $i < $num_lines; $i++) {
      // If the channel was removed, skip and continue on to the next channel.
      if (in_array($i, $removed_fields)) {
        continue;
      }

      $form['channels'][$i] = [
        '#type' => 'fieldset',
      ];

      $form['channels'][$i]['type'] = [
        '#title' => $this->t('Channel'),
        '#type' => 'select',
        '#options' => self::getChannels(),
        '#default_value' => $channels[$i]['type'] ?? NULL,
        '#description' => $this->t('The type of social media channel. This is used to decide which icon to show.'),
      ];

      $form['channels'][$i]['url'] = [
        '#title' => $this->t('URL'),
        '#type' => 'url',
        '#required' => TRUE,
        '#default_value' => $channels[$i]['url'] ?? NULL,
        '#description' => [
          ['#markup' => $this->t('The URL to the social media channel.')],
          ['#markup' => '<br />'],
          ['#markup' => $this->t('Make sure to enter the absolute URL ( e.g. start with https://.. ).')],
        ],
      ];

      $form['channels'][$i]['title'] = [
        '#title' => $this->t('Title'),
        '#type' => 'textfield',
        '#default_value' => $channels[$i]['title'] ?? NULL,
        '#description' => [
          ['#markup' => $this->t('The label for the social media channel.')],
        ],
      ];

      $form['channels'][$i]['icon'] = [
        '#type' => 'managed_file',
        '#title' => 'Icon',
        '#name' => 'icon',
        '#upload_location' => 'public://',
      ];

      // If icon exists then display in form with remove button.
      if (!empty($channels) && $channels[$i]['icon_fid'] != NULL) {
        $form['channels'][$i]['icon']['#value']['fids'] = [$channels[$i]['icon_fid']];
      }

      if ($i > 0) {
        $form['channels'][$i]['actions'] = [
          '#type' => 'submit',
          '#value' => $this->t('Remove channel'),
          '#name' => $i,
          '#limit_validation_errors' => [],
          '#submit' => ['::removeChannel'],
          '#ajax' => [
            'callback' => '::addMoreCallback',
            'wrapper' => 'channels-wrapper',
          ],
        ];
      }
    }

    $form['channels']['actions'] = [
      '#type' => 'actions',
    ];

    $form['channels']['actions']['add_channel'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add channel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::addChannel'],
      '#ajax' => [
        'callback' => '::addMoreCallback',
        'wrapper' => 'channels-wrapper',
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * AJAX-callback to add another channel.
   *
   * @param array $form
   *   The form, passed by reference.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function addMoreCallback(array &$form, FormStateInterface $form_state): array {
    return $form['channels'];
  }

  /**
   * Submit handler for the "add channel" button.
   *
   * @param array $form
   *   The form, passed by reference.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function addChannel(array &$form, FormStateInterface $form_state): void {
    $channels = $form_state->get('channels');

    // Simply increase the number of channels by one
    // as buildForm() will take care of the rest.
    $form_state->set('channels', $channels + 1);
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove channel" button.
   *
   * @param array $form
   *   The form, passed by reference.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function removeChannel(array &$form, FormStateInterface $form_state): void {
    $trigger = $form_state->getTriggeringElement();

    // Get the index of the channel to remove.
    $index = $trigger['#name'];

    // Remove the relevant channel fieldset from $form.
    unset($form['channels'][$index]);

    // Remove the relevant fieldset from the $form_state by fetching it, editing
    // it and then setting it again. This is necessary as Form API does not
    // allow us to directly edit the field.
    $channels = $form_state->getValue('channels');
    unset($channels[$index]);
    $form_state->setValue('channels', $channels);

    // Keep track of the removed channels.
    $removed_fields = $form_state->get('removed_fields');
    $removed_fields[] = $index;
    $form_state->set('removed_fields', $removed_fields);

    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->cleanValues()->getValues();

    $channels = [];

    // Loop through the channels, and store only the necessary information by
    // removing irrelevant and/or empty data.
    foreach ($values['channels'] as $channel) {
      unset($channel['actions']);

      $channel = array_filter($channel);
      if (!empty($channel)) {
        $channels[] = [
          'type' => $channel['type'],
          'url' => $channel['url'],
          'title' => $channel['title'] ?? '',
          'icon_fid' => isset($channel['icon']) ? $channel['icon'][0] : NULL,
        ];
      }
    }

    $config = $this->configFactory()->getEditable(static::CONFIG_NAME);
    $config->set('channels', $channels)->save();
  }

  /**
   * Get the various types of available social media channels.
   *
   * @return array
   *   An array containing channel mapping.
   */
  public static function getChannels(): array {
    return [
      'facebook' => t('Facebook'),
      'instagram' => t('Instagram'),
      'snapchat' => t('Snapchat'),
      'twitter' => t('X'),
      'linkedin' => t('Linkedin'),
      'youtube' => t('Youtube'),
    ];
  }

  /**
   * Get the name of a given type of social media channel.
   *
   * @param string $type
   *   The type of channel.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|null
   *   A translatable channel name, null if channel not defined.
   */
  public static function getChannelName(string $type): ?TranslatableMarkup {
    $channels = self::getChannels();
    return $channels[$type] ?? NULL;
  }

}
