<?php

namespace Drupal\anvil_toolkit_social\Plugin\Block;

use Drupal\anvil_toolkit_social\Form\SocialLinksForm;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\file\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SocialLinksBlock.
 *
 * @Block(
 *  id = "anvil_social_links",
 *  admin_label = @Translation("Social links"),
 *  category = @Translation("Anvil toolkit social links"),
 * )
 */
class SocialLinksBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a SocialLinksBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The social links configuration object.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   The file url generator service.
   */
  public function __construct(
    array $configuration,
          $plugin_id,
          $plugin_definition,
    protected ImmutableConfig $config,
    protected FileUrlGeneratorInterface $fileUrlGenerator
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')->get(SocialLinksForm::CONFIG_NAME),
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [
      '#theme' => 'social_links',
      '#channels' => [],
    ];

    // Fetch the configured channels.
    $channels = $this->config->get('channels') ?? [];
    foreach ($channels as $channel) {
      if (empty($channel['url'])) {
        continue;
      }

      // Override name with the translatable name.
      $channel['name'] = SocialLinksForm::getChannelName($channel['type']);

      // Override file id with file path.
      if ($channel['icon_fid']) {
        // Make sure the File (still) exists.
        if ($file = File::load($channel['icon_fid'])) {
          $channel['icon_path'] = $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri());
        }
      }

      $build['#channels'][] = $channel;
    }

    // Don't bother rendering the block, since there are no channels configured.
    if (!isset($build['#channels']) || empty($build['#channels'])) {
      return [];
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    return Cache::mergeTags(parent::getCacheTags(), [
      'config:' . SocialLinksForm::CONFIG_NAME,
    ]);
  }

}
