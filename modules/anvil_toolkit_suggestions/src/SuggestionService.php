<?php

namespace Drupal\anvil_toolkit_suggestions;

/**
 * Class SuggestionService.
 *
 * @package Drupal\anvil_toolkit_suggestions
 */
class SuggestionService {

  /**
   * Format a string for a valid suggestion.
   *
   * @param string $string
   *   Suggestion.
   *
   * @return string|array|null
   *   A formatted string to use as suggestion.
   */
  public function formatForSuggestion(string $string): string|array|null {
    $lower = strtolower($string);
    return preg_replace('/[\s-]+/', '_', $lower);
  }

}
