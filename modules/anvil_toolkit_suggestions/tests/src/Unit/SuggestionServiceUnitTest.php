<?php

namespace Drupal\Tests\anvil_toolkit_suggestions\Unit;

use Drupal\anvil_toolkit_suggestions\SuggestionService;
use Drupal\Tests\UnitTestCase;

/**
 * SuggestionService unit test class.
 *
 * @group anvil_toolkit_suggestions
 * @coversDefaultClass \Drupal\anvil_toolkit_suggestions\SuggestionService
 */
class SuggestionServiceUnitTest extends UnitTestCase {

  /**
   * The service under test.
   *
   * @var \Drupal\anvil_toolkit_suggestions\SuggestionService
   *   The suggestion service.
   */
  protected SuggestionService $suggestionService;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->suggestionService = new SuggestionService();
  }

  /**
   * Test formatForSuggestion().
   *
   * @dataProvider suggestionsProvider
   */
  public function testFormatForSuggestion(string $value, string $expectation) {
    $this->assertEquals($expectation, $this->suggestionService->formatForSuggestion($value));
  }

  /**
   * Provides values for testing the formatForSuggestion() method.
   *
   * @return array[]
   *   A multidimensional array containing the values.
   */
  public function suggestionsProvider(): array {
    return [
      ['hello-world', 'hello_world'],
      ['helloworld', 'helloworld'],
      ['hello_world', 'hello_world'],
      ['hello_WoRlD', 'hello_world'],
    ];
  }

}
