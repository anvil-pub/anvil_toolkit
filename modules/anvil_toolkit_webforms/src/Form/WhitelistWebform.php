<?php

namespace Drupal\anvil_toolkit_webforms\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Whitelist logic for webforms that shouldn't be stored in configuration.
 */
class WhitelistWebform extends ConfigFormBase {

  const CONFIG_NAME = 'anvil_toolkit_webforms.config.whitelist';
  const FORM_ID = 'anvil_toolkit_webforms_whitelist';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return static::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $whitelist = $this->config(static::CONFIG_NAME)->get('whitelist');
    $whitelist_of_webforms = '';

    // Insert a line break after each value to
    // facilitate proper display within a textarea.
    $whitelist_exists = !is_array($whitelist) ? 0 : count($whitelist);
    if ($whitelist_exists) {
      foreach ($whitelist as $webform) {
        if (!is_array($webform)) {
          $whitelist_of_webforms .= $webform . PHP_EOL;
        }
      }
    }

    $form['whitelist'] = [
      '#title' => $this->t("Webform ID's"),
      '#type' => 'textarea',
      '#default_value' => $whitelist_of_webforms,
      '#description' => $this->t('Define webforms that should be overridden on config import. By default, no webforms will be overridden. For each new webform id, include a <strong>line break</strong>.'),
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->cleanValues()->getValues();

    $normalized_values = $this->normalizeTextareaWhitelist($values['whitelist'] ?? '');

    // Save modified whitelist of webforms.
    $config = $this->config(static::CONFIG_NAME);
    $config->set('whitelist', $normalized_values)->save();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->cleanValues()->getValues();
    $normalized_values = $this->normalizeTextareaWhitelist($values['whitelist'] ?? '');
    foreach ($normalized_values as $normalized_value) {
      if (!str_starts_with($normalized_value, 'webform.webform.')) {
        $form_state->setErrorByName('whitelist', $this->t("Webform config id's should start with 'webform.webform.'"));
      }
    }
  }

  /**
   * Normalize the incoming form values.
   *
   * @param string $value
   *   A string containing textarea input.
   *
   * @return array
   *   An array of normalized values.
   */
  public function normalizeTextareaWhitelist(string $value): array {
    $values = explode(PHP_EOL, $value);
    $values = array_filter($values, function ($value) {
      if (strlen($value)) {
        // Strip whitespace.
        return preg_replace('/\s+/', '', $value);
      }
      return NULL;
    });

    return array_map(function ($value) {
      return trim($value);
    }, $values);
  }

}
