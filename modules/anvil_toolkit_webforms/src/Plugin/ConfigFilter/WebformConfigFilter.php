<?php

namespace Drupal\anvil_toolkit_webforms\Plugin\ConfigFilter;

use Drupal\anvil_toolkit_webforms\Form\WhitelistWebform;
use Drupal\config_filter\Plugin\ConfigFilterBase;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Site\Settings;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an ignore filter that reads partly from the active storage.
 *
 * @ConfigFilter(
 *   id = "config_webform_ignore",
 *   label = "Ignore Webforms config",
 *   weight = 100
 * )
 */
class WebformConfigFilter extends ConfigFilterBase implements ContainerFactoryPluginInterface {

  /**
   * Whether the plugin is disabled via settings.php.
   *
   * @var bool
   */
  protected bool $isDisabled;

  /**
   * Constructs a new WebformConfigFilter.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\StorageInterface $configStorage
   *   The config factory.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    protected StorageInterface $configStorage
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->isDisabled = Settings::get('webform_config_ignore_disabled', FALSE);
  }

  /**
   * List whitelist.
   *
   * @return array
   *   An array of webform id's.
   */
  public static function whitelist(): array {
    $settings = \Drupal::config(WhitelistWebform::CONFIG_NAME);
    return $settings->get('whitelist') ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.storage')
    );
  }

  /**
   * Match a config entity name against the Webform config entities.
   *
   * @param string $config_name
   *   The name of the config entity to match against all ignored entities.
   *
   * @return bool
   *   True, if the config entity is to be ignored, false otherwise.
   */
  protected function matchConfigName(string $config_name): bool {
    // Whitelisted Webforms are _never_ ignored. Which also means they should
    // not be edited by customers, as all changes will always be overridden.
    if (in_array($config_name, self::whitelist())) {
      return FALSE;
    }

    if (str_starts_with($config_name, 'webform.webform.')) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Read from the active configuration.
   *
   * This method will read the configuration from the active config store.
   *
   * @param string $name
   *   The name of the configuration to read.
   * @param mixed $data
   *   The data to be filtered.
   *
   * @return mixed
   *   The data filtered or read from the active storage.
   */
  protected function activeRead(string $name, $data): mixed {
    $active_data = $this->configStorage->read($name);

    // If the active Webform data cannot be found, return
    // data to allow imports of new Webforms.
    if ($active_data) {
      return $active_data;
    }

    return $data;
  }

  /**
   * Read multiple from the active storage.
   *
   * @param array $names
   *   The names of the configuration to read.
   * @param array $data
   *   The data to filter.
   *
   * @return array
   *   The new data.
   */
  protected function activeReadMultiple(array $names, array $data): array {
    $filtered_data = [];
    foreach ($names as $name) {
      $filtered_data[$name] = $this->activeRead($name, $data[$name]);
    }

    return $filtered_data;
  }

  /**
   * {@inheritdoc}
   */
  public function filterRead($name, $data) {

    if ($this->isDisabled) {
      return $data;
    }

    // Read from the active storage when the name is in the ignored list.
    if ($this->matchConfigName($name)) {
      return $this->activeRead($name, $data);
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function filterWrite($name, $data) {
    if ($this->isDisabled) {
      return $data;
    }

    // When encountering ignored webform configuration,
    // don't write it to file system on config export.
    if ($this->matchConfigName($name)) {
      return FALSE;
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function filterExists($name, $exists): bool {
    if ($this->isDisabled) {
      return $exists;
    }

    // A name exists if it is ignored and exists in the active storage.
    return $exists || ($this->matchConfigName($name) && $this->configStorage->exists($name));
  }

  /**
   * {@inheritdoc}
   */
  public function filterReadMultiple(array $names, array $data): array {
    if ($this->isDisabled) {
      return $data;
    }

    // Limit the names which are read from the active storage.
    $names = array_filter($names, [$this, 'matchConfigName']);
    $active_data = $this->activeReadMultiple($names, $data);

    // Return the data with merged in active data.
    return array_merge($data, $active_data);
  }

  /**
   * {@inheritdoc}
   */
  public function filterListAll($prefix, array $data): array {
    if ($this->isDisabled) {
      return $data;
    }

    $active_names = $this->configStorage->listAll($prefix);

    // Filter out only Webform config names.
    $active_names = array_filter($active_names, [$this, 'matchConfigName']);

    // Return the data with the active names which are ignored merged in.
    return array_unique(array_merge($data, $active_names));
  }

  /**
   * {@inheritdoc}
   */
  public function filterCreateCollection($collection): ConfigFilterBase {
    return new static($this->configuration, $this->pluginId, $this->pluginDefinition, $this->configStorage->createCollection($collection));
  }

  /**
   * {@inheritdoc}
   */
  public function filterGetAllCollectionNames(array $collections): array {
    // Add active collection names as there could be ignored config in them.
    return array_merge($collections, $this->configStorage->getAllCollectionNames());
  }

}
